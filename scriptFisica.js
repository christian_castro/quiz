let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let fisOne = document.getElementById("fisOne")
let calorOne = document.getElementById("calorOne")
let calorTwo = document.getElementById("calorTwo")
let calorThree = document.getElementById("calorThree")


// Segunda Pergunta //
let fisTwo = document.getElementById("fisTwo")
let calorFour = document.getElementById("calorFour")
let calorFive = document.getElementById("calorFive")
let calorSix = document.getElementById("calorSix")

// Terceiro Pergunta //
let fisThree = document.getElementById("fisThree")
let solOne = document.getElementById("solOne")
let solTwo = document.getElementById("solTwo")
let solThree = document.getElementById("solThree")

// Quarta Pergunta //
let fisFour = document.getElementById("fisFour")
let fogoOne = document.getElementById("fogoOne")
let fogoTwo = document.getElementById("fogoTwo")
let fogoThree = document.getElementById("fogoThree")

// Quinta Pergunta //
let fisFive = document.getElementById("fisFive")
let luzOne = document.getElementById("luzOne")
let luzTwo = document.getElementById("luzTwo")
let luzThree = document.getElementById("luzThree")

// Sexta Pergunta //
let fisSix = document.getElementById("fisSix")
let forcaOne = document.getElementById("forcaOne")
let forcaTwo = document.getElementById("forcaTwo")
let forcaThree = document.getElementById("forcaThree")

// Sétima Pergunta //
let fisSeven = document.getElementById("fisSeven")
let somOne = document.getElementById("somOne")
let somTwo = document.getElementById("somTwo")
let somThree = document.getElementById("somThree")

// Oitava Pergunta //
let fisEight = document.getElementById("fisEight")
let planetaOne = document.getElementById("planetaOne")
let planetaTwo = document.getElementById("planetaTwo")
let planetaThree = document.getElementById("planetaThree")

// Nona  Pergunta //
let fisNine = document.getElementById("fisNine")
let tempoOne = document.getElementById("tempoOne")
let tempoTwo = document.getElementById("tempoTwo")
let tempoThree = document.getElementById("tempoThree")

// Décima  Pergunta //
let fisTen = document.getElementById("fisTen")
let romanoOne = document.getElementById("romanoOne")
let romanoTwo = document.getElementById("romanoTwo")
let romanoThree = document.getElementById("romanoThree")

// -------------------------------------------------- //

let emoji = document.getElementById("emoji")

// Cronometro // 
 let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    fisOne.style.display = "none"
    fisTwo.style.display = "none"
    fisThree.style.display = "none"
    fisFour.style.display = "none"
    fisFive.style.display = "none"
    fisSix.style.display = "none"
    fisSeven.style.display = "none"
    fisEight.style.display = "none"
    fisNine.style.display = "none"
    fisTen.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Tempo acabou! Você acertou ${soma}`
    acertando.style.display = "none"
}
} 


// Primeira Pergunta //
    
    fisOne.addEventListener("click", (evt) => {
        if(evt.target === calorOne) {
            fisOne.style.display = "none"
            fisTwo.style.display = "block"
        } 

        if(evt.target === calorTwo) {
            soma = soma + 1
            fisOne.style.display = "none"
            fisTwo.style.display = "block"    
        }

        if(evt.target === calorThree) {
            fisOne.style.display = "none"
            fisTwo.style.display = "block"
            
        }
    })


    // Segunda Pergunta //

    fisTwo.addEventListener("click", (evt) => {
        if(evt.target === calorFour) {
            soma = soma + 1
            fisTwo.style.display = "none"
            fisThree.style.display = "block"
        } 
        
        if(evt.target === calorFive) {
            fisTwo.style.display = "none"
            fisThree.style.display = "block"
           
        }

        if(evt.target === calorSix) {
            fisTwo.style.display = "none"
            fisThree.style.display = "block"
            
        }
    })


    // Terceira Pergunta //
    fisThree.addEventListener("click", (evt) => {
        if(evt.target === solOne) {
            fisThree.style.display = "none"
            fisFour.style.display = "block"
        } 
        
        if(evt.target === solTwo) {
            soma = soma + 1
            fisThree.style.display = "none"
            fisFour.style.display = "block"        
        }

        if(evt.target === solThree) {
            fisThree.style.display = "none"
            fisFour.style.display = "block"
        }
    })


      // Quarta Pergunta //
      fisFour.addEventListener("click", (evt) => {
        if(evt.target === fogoOne) {
            fisFour.style.display = "none"
            fisFive.style.display = "block"
        } 
        
        if(evt.target === fogoTwo) {
            fisFour.style.display = "none"
            fisFive.style.display = "block"
        }
        
        if(evt.target === fogoThree) {
            soma = soma + 1
            fisFour.style.display = "none"
            fisFive.style.display = "block"
        }
    })

    // Quinta Pergunta //
    fisFive.addEventListener("click", (evt) => {
        if(evt.target === luzOne) {
            fisFive.style.display = "none"
            fisSix.style.display = "block"
        } 
        
        if(evt.target === luzTwo) {
            fisFive.style.display = "none"
            fisSix.style.display = "block"
        }
        
        if(evt.target === luzThree) {
            soma = soma + 1
            fisFive.style.display = "none"
            fisSix.style.display = "block"
        }
    })

    // Sexta Pergunta //
    fisSix.addEventListener("click", (evt) => {
        if(evt.target === forcaOne) {
            soma = soma + 1
            fisSix.style.display = "none"
            fisSeven.style.display = "block"
        } 
        
        if(evt.target === forcaTwo) {
            fisSix.style.display = "none"
            fisSeven.style.display = "block"
        }
        
        if(evt.target === forcaThree) {
            fisSix.style.display = "none"
            fisSeven.style.display = "block"
        }
    })

    // Sétima Pergunta //
    fisSeven.addEventListener("click", (evt) => {
        if(evt.target === somOne) {
            fisSeven.style.display = "none"
            fisEight.style.display = "block"
        } 
        
        if(evt.target === somTwo) {
            fisSeven.style.display = "none"
            fisEight.style.display = "block"
        }
        
        if(evt.target === somThree) {
            soma = soma + 1
            fisSeven.style.display = "none"
            fisEight.style.display = "block"
        }
    })


    // Oitava Pergunta //
    fisEight.addEventListener("click", (evt) => {
        if(evt.target === planetaOne) {
            fisEight.style.display = "none"
            fisNine.style.display = "block"
        } 
        
        if(evt.target === planetaTwo) {
            soma = soma + 1
            fisEight.style.display = "none"
            fisNine.style.display = "block"
        }
        
        if(evt.target === planetaThree) {
            fisEight.style.display = "none"
            fisNine.style.display = "block"
        }
    })

    // Nona Pergunta //
    fisNine.addEventListener("click", (evt) => {
        if(evt.target === tempoOne) {
            fisNine.style.display = "none"
            fisTen.style.display = "block"
        } 
        
        if(evt.target === tempoTwo) {
            fisNine.style.display = "none"
            fisTen.style.display = "block"
        }
        
        if(evt.target === tempoThree) {
            soma = soma + 1
            fisNine.style.display = "none"
            fisTen.style.display = "block"
        }
    })

    // Décima Pergunta //
    fisTen.addEventListener("click", (evt) => {
        if(evt.target === romanoOne) {
            soma = soma + 1
            fisTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        } 
        
        if(evt.target === romanoTwo) {
            fisTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
        
        if(evt.target === romanoThree) {  
            fisTen.style.display = "none"   
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
    })

// Função Voltar na página inicial // 
next.addEventListener("click" , (evt) => { 
    window.location.href = "disciplinas.html" 
  }) 
  
  
  // Função Resetar Jogo //
   limpar.addEventListener("click", () => {
     window.location.href = "fisica.html"
  }) 

  
// Função de calcular pontuação //
acertando.addEventListener("click", () => {
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`
})

  