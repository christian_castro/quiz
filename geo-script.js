let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let geoOne = document.getElementById("geoOne")
let paisOne = document.getElementById("paisOne")
let paisTwo = document.getElementById("paisTwo")
let paisThree = document.getElementById("paisThree")


// Segunda Pergunta //
let geoTwo = document.getElementById("geoTwo")
let expecOne = document.getElementById("expecOne")
let expecTwo = document.getElementById("expecTwo")
let expecThree = document.getElementById("expecThree")

// Terceiro Pergunta //
let geoThree = document.getElementById("geoThree")
let montanhaOne = document.getElementById("montanhaOne")
let montanhaTwo = document.getElementById("montanhaTwo")
let montanhaThree = document.getElementById("montanhaThree")

// Quarta Pergunta //
let geoFour = document.getElementById("geoFour")
let capitalOne = document.getElementById("capitalOne")
let capitalTwo = document.getElementById("capitalTwo")
let capitalThree = document.getElementById("capitalThree")

// Quinta Pergunta //
let geoFive = document.getElementById("geoFive")
let capitalFour = document.getElementById("capitalFour")
let capitalFive = document.getElementById("capitalFive")
let capitalSix = document.getElementById("capitalSix")

// Sexta Pergunta //
let geoSix = document.getElementById("geoSix")
let continentalOne = document.getElementById("continentalOne")
let continentalTwo = document.getElementById("continentalTwo")
let continentalThree = document.getElementById("continentalThree")

// Sétima Pergunta //
let geoSeven = document.getElementById("geoSeven")
let acreOne = document.getElementById("acreOne")
let acreTwo = document.getElementById("acreTwo")
let acreThree = document.getElementById("acreThree")

// Oitava Pergunta //
let geoEight = document.getElementById("geoEight")
let baciaOne = document.getElementById("baciaOne")
let baciaTwo = document.getElementById("baciaTwo")
let baciaThree = document.getElementById("baciaThree")

// Nona  Pergunta //
let geoNine = document.getElementById("geoNine")
let fronteiraOne = document.getElementById("fronteiraOne")
let fronteiraTwo = document.getElementById("fronteiraTwo")
let fronteiraThree = document.getElementById("fronteiraThree")

// Décima  Pergunta //
let geoTen = document.getElementById("geoTen")
let countryOne = document.getElementById("countryOne")
let countryTwo = document.getElementById("countryTwo")
let countryThree = document.getElementById("countryThree")

// -------------------------------------------------- //

// Cronometro // 
  let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    geoOne.style.display = "none"
    geoTwo.style.display = "none"
    geoThree.style.display = "none"
    geoFour.style.display = "none"
    geoFive.style.display = "none"
    geoSix.style.display = "none"
    geoSeven.style.display = "none"
    geoEight.style.display = "none"
    geoNine.style.display = "none"
    geoTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`
}
} 


    // Primeira Pergunta //
    geoOne.addEventListener("click", (evt) => {
        if(evt.target === paisOne) {
            geoOne.style.display = "none"
            geoTwo.style.display = "block"
           
        } 

        if(evt.target === paisTwo) {
            geoOne.style.display = "none"
            geoTwo.style.display = "block"
        }
        
        if(evt.target === paisThree) {
            soma = soma + 1
            geoOne.style.display = "none"
            geoTwo.style.display = "block"  
        }
    })

    // Segunda Pergunta //
    geoTwo.addEventListener("click", (evt) => {
        if(evt.target === expecOne) {
            geoTwo.style.display = "none"
            geoThree.style.display = "block"  
        } 

        if(evt.target === expecTwo) {
            geoTwo.style.display = "none"
            geoThree.style.display = "block"  
        }
        
        if(evt.target === expecThree) {
            soma = soma + 1
            geoTwo.style.display = "none"
            geoThree.style.display = "block"  
            
        }
    })

    // Terceira Pergunta //
    geoThree.addEventListener("click", (evt) => {
        if(evt.target === montanhaOne) {
            soma = soma + 1
            geoThree.style.display = "none"
            geoFour.style.display = "block"
        } 
        
        if(evt.target === montanhaTwo) {
            geoThree.style.display = "none"
            geoFour.style.display = "block"
        }
        
        if(evt.target === montanhaThree) {
            geoThree.style.display = "none"
            geoFour.style.display = "block"
            
        }
    })   


    // Quarta Pergunta //
    geoFour.addEventListener("click", (evt) => {
        if(evt.target === capitalOne) {
            geoFour.style.display = "none"
            geoFive.style.display = "block"
        } 
        
        if(evt.target === capitalTwo) {
            soma = soma + 1
            geoFour.style.display = "none"
            geoFive.style.display = "block"
        }
        
        if(evt.target === capitalThree) {
            geoFour.style.display = "none"
            geoFive.style.display = "block"
            
        }
    }) 


    // Quinta Pergunta //
    geoFive.addEventListener("click", (evt) => {
        if(evt.target === capitalFour) {
            soma = soma + 1
            geoFive.style.display = "none"
            geoSix.style.display = "block"
        } 
        
        if(evt.target === capitalFive) {
            geoFive.style.display = "none"
            geoSix.style.display = "block"
        }
        
        if(evt.target === capitalSix) {
            geoFive.style.display = "none"
            geoSix.style.display = "block"
            
        }
    }) 


    // Sexta Pergunta //
    geoSix.addEventListener("click", (evt) => {
        if(evt.target === continentalOne) {
            geoSix.style.display = "none"
            geoSeven.style.display = "block"
        } 
        
        if(evt.target === continentalTwo) {
            geoSix.style.display = "none"
            geoSeven.style.display = "block"
        }
        
        if(evt.target === continentalThree) {
            soma = soma + 1
            geoSix.style.display = "none"
            geoSeven.style.display = "block"
            
        }
    }) 


     // Sétima Pergunta //
     geoSeven.addEventListener("click", (evt) => {
        if(evt.target === acreOne) {
            soma = soma + 1
            geoSeven.style.display = "none"
            geoEight.style.display = "block"
        } 
        
        if(evt.target === acreTwo) {
            geoSeven.style.display = "none"
            geoEight.style.display = "block"
        }
        
        if(evt.target === acreThree) {
            geoSeven.style.display = "none"
            geoEight.style.display = "block"
            
        }
    }) 

    // Oitava Pergunta //
    geoEight.addEventListener("click", (evt) => {
        if(evt.target === baciaOne) {
            geoEight.style.display = "none"
            geoNine.style.display = "block"
        } 
        
        if(evt.target === baciaTwo) {
            soma = soma + 1
            geoEight.style.display = "none"
            geoNine.style.display = "block"
        }
        
        if(evt.target === baciaThree) {
            geoEight.style.display = "none"
            geoNine.style.display = "block"         
        }
    }) 


    // Nona Pergunta //
    geoNine.addEventListener("click", (evt) => {
        if(evt.target === fronteiraOne) {
            soma = soma + 1
            geoNine.style.display = "none"
            geoTen.style.display = "block"
        } 
        
        if(evt.target === fronteiraTwo) {
            geoNine.style.display = "none"
            geoTen.style.display = "block"
        }
        
        if(evt.target === fronteiraThree) {
            geoNine.style.display = "none"
            geoTen.style.display = "block"
            
        }
    }) 

     // Décima Pergunta //
     geoTen.addEventListener("click", (evt) => {
        if(evt.target === countryOne) {
            geoTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        } 
        
        if(evt.target === countryTwo) {
            geoTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
        
        if(evt.target === countryThree) {
            soma = soma + 1
            geoTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
    }) 


    let popUp = document.getElementById("popUp");
    let abrir = document.getElementById("abrir")
    let fechar = document.getElementById("fechar")
    
     // Função pular no momento desativada //
    /* fechar.addEventListener("click", () => {
        popUp.style.display = "none"
    }) */
    
    
    // Função Voltar na página inicial // 
    next.addEventListener("click" , (evt) => { 
        window.location.href = "disciplinas.html" 
    }) 
    
    
    // Função Resetar Jogo //
    limpar.addEventListener("click", () => {
        window.location.href = "geografia.html" 
    }) 
    
    // Função de pular no momento desativada  //
  /*  verificar.addEventListener("click", (evt) => {
        alert(`Você acertou ${soma}`)
    })  */
    
    // Função de calcular pontuação //
    acertando.addEventListener("click", () => {
        acertando.style.display = "none"
        popUp.style.display = "block"
        tudo.appendChild(popUp)
        popUp.textContent = `Você acertou ${soma}`
    })
    

  