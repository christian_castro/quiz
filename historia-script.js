let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let histOne = document.getElementById("histOne")
let secOne = document.getElementById("secOne")
let secTwo = document.getElementById("secTwo")
let secThree = document.getElementById("secThree")


// Segunda Pergunta //
let histTwo = document.getElementById("histTwo")
let nobelOne = document.getElementById("nobelOne")
let nobelTwo = document.getElementById("nobelTwo")
let nobelThree = document.getElementById("nobelThree")

// Terceiro Pergunta //
let histThree = document.getElementById("histThree")
let raioOne = document.getElementById("raioOne")
let raioTwo = document.getElementById("raioTwo")
let raioThree = document.getElementById("raioThree")

// Quarta Pergunta //
let histFour = document.getElementById("histFour")
let minasOne = document.getElementById("minasOne")
let minasTwo = document.getElementById("minasTwo")
let minasThree = document.getElementById("minasThree")

// Quinta Pergunta //
let histFive = document.getElementById("histFive")
let saddamOne = document.getElementById("saddamOne")
let saddamTwo = document.getElementById("saddamTwo")
let saddamThree = document.getElementById("saddamThree")

// Sexta Pergunta //
let histSix = document.getElementById("histSix")
let indOne = document.getElementById("indOne")
let indTwo = document.getElementById("indTwo")
let indThree = document.getElementById("indThree")

// Sétima Pergunta //
let histSeven = document.getElementById("histSeven")
let anosOne = document.getElementById("anosOne")
let anosTwo = document.getElementById("anosTwo")
let anosThree = document.getElementById("anosThree")

// Oitava Pergunta //
let histEight = document.getElementById("histEight")
let virusOne = document.getElementById("virusOne")
let virusTwo = document.getElementById("virusTwo")
let virusThree = document.getElementById("virusThree")

// Nona  Pergunta //
let histNine = document.getElementById("histNine")
let astroOne = document.getElementById("astroOne")
let astroTwo = document.getElementById("astroTwo")
let astroThree = document.getElementById("astroThree")

// Décima  Pergunta //
let histTen = document.getElementById("histTen")
let inventOne = document.getElementById("inventOne")
let inventTwo = document.getElementById("inventTwo")
let inventThree = document.getElementById("inventThree")

// -------------------------------------------------- //

// Cronometro // 
  let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    histOne.style.display = "none"
    histTwo.style.display = "none"
    histThree.style.display = "none"
    histFour.style.display = "none"
    histFive.style.display = "none"
    histSix.style.display = "none"
    histSeven.style.display = "none"
    histEight.style.display = "none"
    histNine.style.display = "none"
    histTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`
} 
} 

    // Primeira Pergunta //
    histOne.addEventListener("click", (evt) => {
        if(evt.target === secOne) {
            histOne.style.display = "none"
            histTwo.style.display = "block"
           
        } 

        if(evt.target === secTwo) {
            histOne.style.display = "none"
            histTwo.style.display = "block"
        }
        
        if(evt.target === secThree) {
            soma = soma + 1
            histOne.style.display = "none"
            histTwo.style.display = "block"  
        }
    })

    // Segunda Pergunta //
    histTwo.addEventListener("click", (evt) => {
        if(evt.target === nobelOne) {
            soma = soma + 1
            histTwo.style.display = "none"
            histThree.style.display = "block"  
        } 

        if(evt.target === nobelTwo) {
            histTwo.style.display = "none"
            histThree.style.display = "block"  
        }
        
        if(evt.target === nobelThree) {
            histTwo.style.display = "none"
            histThree.style.display = "block"  
            
        }
    })

    // Terceira Pergunta //
    histThree.addEventListener("click", (evt) => {
        if(evt.target === raioOne) {
            soma = soma + 1
            histThree.style.display = "none"
            histFour.style.display = "block"
        } 
        
        if(evt.target === raioTwo) {
            histThree.style.display = "none"
            histFour.style.display = "block"
        }
        
        if(evt.target === raioThree) {
            histThree.style.display = "none"
            histFour.style.display = "block"
            
        }
    })   


    // Quarta Pergunta //
    histFour.addEventListener("click", (evt) => {
        if(evt.target === minasOne) {
            histFour.style.display = "none"
            histFive.style.display = "block"
        } 
        
        if(evt.target === minasTwo) {
            
            histFour.style.display = "none"
            histFive.style.display = "block"
        }
        
        if(evt.target === minasThree) {
            soma = soma + 1
            histFour.style.display = "none"
            histFive.style.display = "block"
            
        }
    }) 


    // Quinta Pergunta //
    histFive.addEventListener("click", (evt) => {
        if(evt.target === saddamOne) {
            soma = soma + 1
            histFive.style.display = "none"
            histSix.style.display = "block"
        } 
        
        if(evt.target === saddamTwo) {
            histFive.style.display = "none"
            histSix.style.display = "block"
        }
        
        if(evt.target === saddamThree) {
            histFive.style.display = "none"
            histSix.style.display = "block"
            
        }
    }) 


    // Sexta Pergunta //
    histSix.addEventListener("click", (evt) => {
        if(evt.target === indOne) {
            histSix.style.display = "none"
            histSeven.style.display = "block"
        } 
        
        if(evt.target === indTwo) {
            soma = soma + 1
            histSix.style.display = "none"
            histSeven.style.display = "block"
        }
        
        if(evt.target === indThree) {
            histSix.style.display = "none"
            histSeven.style.display = "block"
            
        }
    }) 


     // Sétima Pergunta //
     histSeven.addEventListener("click", (evt) => {
        if(evt.target === anosOne) {
            histSeven.style.display = "none"
            histEight.style.display = "block"
        } 
        
        if(evt.target === anosTwo) {
            histSeven.style.display = "none"
            histEight.style.display = "block"
        }
        
        if(evt.target === anosThree) {
            soma = soma + 1
            histSeven.style.display = "none"
            histEight.style.display = "block"
            
        }
    }) 

    // Oitava Pergunta //
    histEight.addEventListener("click", (evt) => {
        if(evt.target === virusOne) {
            histEight.style.display = "none"
            histNine.style.display = "block"
        } 
        
        if(evt.target === virusOne) {
            histEight.style.display = "none"
            histNine.style.display = "block"
        }
        
        if(evt.target === virusThree) {
            soma = soma + 1
            histEight.style.display = "none"
            histNine.style.display = "block"         
        }
    }) 


    // Nona Pergunta //
    histNine.addEventListener("click", (evt) => {
        if(evt.target === astroOne) {
            histNine.style.display = "none"
            histTen.style.display = "block"
        } 
        
        if(evt.target === astroTwo) {
            histNine.style.display = "none"
            histTen.style.display = "block"
        }
        
        if(evt.target === astroThree) {
            soma = soma + 1
            histNine.style.display = "none"
            histTen.style.display = "block"
            
        }
    }) 

     // Décima Pergunta //
     histTen.addEventListener("click", (evt) => {
        if(evt.target === inventOne) {
            soma = soma + 1
            histTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        } 
        
        if(evt.target === inventTwo) {
            histTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
        
        if(evt.target === inventThree) {
            histTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
    }) 


    let popUp = document.getElementById("popUp");
    let abrir = document.getElementById("abrir")
    let fechar = document.getElementById("fechar")
    
     // Função pular no momento desativada //
    /* fechar.addEventListener("click", () => {
        popUp.style.display = "none"
    }) */
    
    
    // Função Voltar na página inicial // 
    next.addEventListener("click" , (evt) => { 
        window.location.href = "disciplinas.html" 
    }) 
    
    
    // Função Resetar Jogo //
    limpar.addEventListener("click", () => {
        window.location.href = "historia.html"
    }) 
    
    // Função de pular no momento desativada  //
  /*  verificar.addEventListener("click", (evt) => {
        alert(`Você acertou ${soma}`)
    })  */
    
    // Função de calcular pontuação //
    acertando.addEventListener("click", () => {
        acertando.style.display = "none"
        popUp.style.display = "block"
        tudo.appendChild(popUp)
        popUp.textContent = `Você acertou ${soma}`
    })
    