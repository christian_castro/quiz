let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let engOne = document.getElementById("engOne")
let colorOne = document.getElementById("colorOne")
let colorTwo = document.getElementById("colorTwo")
let colorThree = document.getElementById("colorThree")


// Segunda Pergunta //
let engTwo = document.getElementById("engTwo")
let pronomeOne = document.getElementById("pronomeOne")
let pronomeTwo = document.getElementById("pronomeTwo")
let pronomeThree = document.getElementById("pronomeThree")

// Terceiro Pergunta //
let engThree = document.getElementById("engThree")
let completeOne = document.getElementById("completeOne")
let completeTwo = document.getElementById("completeTwo")
let completeThree = document.getElementById("completeThree")

// Quarta Pergunta //
let engFour = document.getElementById("engFour")
let goodOne = document.getElementById("goodOne")
let goodTwo = document.getElementById("goodTwo")
let goodThree = document.getElementById("goodThree")

// Quinta Pergunta //
let engFive = document.getElementById("engFive")
let calcaOne = document.getElementById("calcaOne")
let calcaTwo = document.getElementById("calcaTwo")
let calcaThree = document.getElementById("calcaThree")

// Sexta Pergunta //
let engSix = document.getElementById("engSix")
let paislOne = document.getElementById("paisOne")
let paislTwo = document.getElementById("paisTwo")
let paisThree = document.getElementById("paisThree")

// Sétima Pergunta //
let engSeven = document.getElementById("engSeven")
let thanksOne = document.getElementById("thanksOne")
let thanksTwo = document.getElementById("thanksTwo")
let thanksThree = document.getElementById("thanksThree")

// Oitava Pergunta //
let engEight = document.getElementById("engEight")
let rioOne = document.getElementById("rioOne")
let rioTwo = document.getElementById("rioTwo")
let rioThree = document.getElementById("rioThree")

// Nona  Pergunta //
let engNine = document.getElementById("engNine")
let momOne = document.getElementById("momOne")
let momTwo = document.getElementById("momTwo")
let momThree = document.getElementById("momThree")

// Décima  Pergunta //
let engTen = document.getElementById("engTen")
let fluenteOne = document.getElementById("fluenteOne")
let fluenteTwo = document.getElementById("fluenteTwo")
let fluenteThree = document.getElementById("fluenteThree")

// -------------------------------------------------- //

// Cronometro // 
  let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    engOne.style.display = "none"
    engTwo.style.display = "none"
    engThree.style.display = "none"
    engFour.style.display = "none"
    engFive.style.display = "none"
    engSix.style.display = "none"
    engSeven.style.display = "none"
    engEight.style.display = "none"
    engNine.style.display = "none"
    engTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`

} 
} 

    // Primeira Pergunta //
    engOne.addEventListener("click", (evt) => {
        if(evt.target === colorOne) {
            engOne.style.display = "none"
            engTwo.style.display = "block"
           
        } 

        if(evt.target === colorTwo) {
            engOne.style.display = "none"
            engTwo.style.display = "block"
        }
        
        if(evt.target === colorThree) {
            soma = soma + 1
            engOne.style.display = "none"
            engTwo.style.display = "block"  
        }
    })

    // Segunda Pergunta //
    engTwo.addEventListener("click", (evt) => {
        if(evt.target === pronomeOne) {
            engTwo.style.display = "none"
            engThree.style.display = "block"  
        } 

        if(evt.target === pronomeTwo) {
            engTwo.style.display = "none"
            engThree.style.display = "block"  
        }
        
        if(evt.target === pronomeThree) {
            soma = soma + 1
            engTwo.style.display = "none"
            engThree.style.display = "block"  
            
        }
    })

    // Terceira Pergunta //
    engThree.addEventListener("click", (evt) => {
        if(evt.target === completeOne) {
            soma = soma + 1
            engThree.style.display = "none"
            engFour.style.display = "block"
        } 
        
        if(evt.target === completeTwo) {
            engThree.style.display = "none"
            engFour.style.display = "block"
        }
        
        if(evt.target === completeThree) {
            engThree.style.display = "none"
            engFour.style.display = "block"
            
        }
    })   


    // Quarta Pergunta //
    engFour.addEventListener("click", (evt) => {
        if(evt.target === goodOne) {
            engFour.style.display = "none"
            engFive.style.display = "block"
        } 
        
        if(evt.target === goodTwo) {
            
            engFour.style.display = "none"
            engFive.style.display = "block"
        }
        
        if(evt.target === goodThree) {
            soma = soma + 1
            engFour.style.display = "none"
            engFive.style.display = "block"
            
        }
    }) 


    // Quinta Pergunta //
    engFive.addEventListener("click", (evt) => {
        if(evt.target === calcaOne) {
            engFive.style.display = "none"
            engSix.style.display = "block"
        } 
        
        if(evt.target === calcaTwo) {
            soma = soma + 1
            engFive.style.display = "none"
            engSix.style.display = "block"
        }
        
        if(evt.target === calcaThree) {
            engFive.style.display = "none"
            engSix.style.display = "block"
            
        }
    }) 


    // Sexta Pergunta //
    engSix.addEventListener("click", (evt) => {
        if(evt.target === paisOne) {
            engSix.style.display = "none"
            engSeven.style.display = "block"
        } 
        
        if(evt.target === paisTwo) {
            engSix.style.display = "none"
            engSeven.style.display = "block"
        }
        
        if(evt.target === paisThree) {
            soma = soma + 1
            engSix.style.display = "none"
            engSeven.style.display = "block"
            
        }
    }) 


     // Sétima Pergunta //
     engSeven.addEventListener("click", (evt) => {
        if(evt.target === thanksOne) {
            soma = soma + 1
            engSeven.style.display = "none"
            engEight.style.display = "block"
        } 
        
        if(evt.target === thanksTwo) {
            engSeven.style.display = "none"
            engEight.style.display = "block"
        }
        
        if(evt.target === thanksThree) {
            engSeven.style.display = "none"
            engEight.style.display = "block"
            
        }
    }) 

    // Oitava Pergunta //
    engEight.addEventListener("click", (evt) => {
        if(evt.target === rioOne) {
            soma = soma + 1
            engEight.style.display = "none"
            engNine.style.display = "block"
        } 
        
        if(evt.target === rioTwo) {
            engEight.style.display = "none"
            engNine.style.display = "block"
        }
        
        if(evt.target === rioThree) {
            engEight.style.display = "none"
            engNine.style.display = "block"         
        }
    }) 


    // Nona Pergunta //
    engNine.addEventListener("click", (evt) => {
        if(evt.target === momOne) {
            soma = soma + 1
            engNine.style.display = "none"
            engTen.style.display = "block"
        } 
        
        if(evt.target === momTwo) {
            engNine.style.display = "none"
            engTen.style.display = "block"
        }
        
        if(evt.target === momThree) {
            engNine.style.display = "none"
            engTen.style.display = "block"
            
        }
    }) 

     // Décima Pergunta //
     engTen.addEventListener("click", (evt) => {
        if(evt.target === fluenteOne) {
            soma = soma + 1
            engTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        } 
        
        if(evt.target === fluenteTwo) {
            engTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
        
        if(evt.target === fluenteThree) {
            engTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
    }) 


    let popUp = document.getElementById("popUp");
    let abrir = document.getElementById("abrir")
    let fechar = document.getElementById("fechar")
    
     // Função pular no momento desativada //
    /* fechar.addEventListener("click", () => {
        popUp.style.display = "none"
    }) */
    
    
    // Função Voltar na página inicial // 
    next.addEventListener("click" , (evt) => { 
        window.location.href = "disciplinas.html" 
    }) 
    
    
    // Função Resetar Jogo //
    limpar.addEventListener("click", () => {
        window.location.href = "ingles.html"
    }) 
    
    // Função de pular no momento desativada  //
  /*  verificar.addEventListener("click", (evt) => {
        alert(`Você acertou ${soma}`)
    })  */
    
    // Função de calcular pontuação //
    acertando.addEventListener("click", () => {
        acertando.style.display = "none"
        popUp.style.display = "block"
        tudo.appendChild(popUp)
        popUp.textContent = `Você acertou ${soma}`
    })
    