let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let quimUm = document.getElementById("quimUm")
let verdadeiro = document.getElementById("verdadeiro")
let falso = document.getElementById("falso")

// Segunda Pergunta //
let quimTwo = document.getElementById("quimTwo")
let estomago = document.getElementById("estomago")
let figado = document.getElementById("figado")
let pancreas = document.getElementById("pancreas")

// Terceiro Pergunta //
let quimThree = document.getElementById("quimThree")
let vaporizacao = document.getElementById("vaporizacao")
let condesacao = document.getElementById("condesacao")
let fusao = document.getElementById("fusao")

// Quarta Pergunta //
let quimFour = document.getElementById("quimFour")
let agua = document.getElementById("agua")
let hidro = document.getElementById("hidro")
let oxi = document.getElementById("oxi")

// Quinta Pergunta //
let quimFive = document.getElementById("quimFive")
let fusaoOne = document.getElementById("fusaoOne")
let fusaoTwo = document.getElementById("fusaoTwo")
let fusaoThree = document.getElementById("fusaoThree")

// Sexta Pergunta //
let quimSix = document.getElementById("quimSix")
let misturaOne = document.getElementById("misturaOne")
let misturaTwo = document.getElementById("misturaTwo")
let misturaThree = document.getElementById("misturaThree")

// Sétima Pergunta //
let quimSeven = document.getElementById("quimSeven")
let energiaOne = document.getElementById("energiaOne")
let energiaTwo = document.getElementById("energiaTwo")
let energiaThree = document.getElementById("energiaThree")

// Oitava Pergunta //
let quimEight = document.getElementById("quimEight")
let formulaOne = document.getElementById("formulaOne")
let formulaTwo = document.getElementById("formulaTwo")
let formulaThree = document.getElementById("formulaThree")

// Nona  Pergunta //
let quimNine = document.getElementById("quimNine")
let fotoOne = document.getElementById("fotoOne")
let fotoTwo = document.getElementById("fotoTwo")
let fotoThree = document.getElementById("fotoThree")

// Décima  Pergunta //
let quimTen = document.getElementById("quimTen")
let dinoOne = document.getElementById("dinoOne")
let dinoTwo = document.getElementById("dinoTwo")
let dinoThree = document.getElementById("dinoThree")

let counter = document.getElementById("counter")

// -------------------------------------------------- //

let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos === 0) {
    counter.style.display = "none";
    quimUm.style.display = "none"
    quimTwo.style.display = "none"
    quimThree.style.display = "none"
    quimFour.style.display = "none"
    quimFive.style.display = "none"
    quimSix.style.display = "none"
    quimSeven.style.display = "none"
    quimEight.style.display = "none"
    quimNine.style.display = "none"
    quimTen.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Tempo acabou! Você acertou ${soma}`
    acertando.style.display = "none"
    
    // Primeira Pergunta //  
} 
    if(segundos < 0) {
        counter.style.display = "none";
    }
}


    // Primeira Pergunta //
    quimUm.addEventListener("click", (evt) => {
        if(evt.target === verdadeiro) {
            quimUm.style.display = "none"
            quimTwo.style.display = "block" 
        } 
        
        if(evt.target === falso) {
            soma = soma + 1
            quimUm.style.display = "none"
            quimTwo.style.display = "block"
        }
    })

    

    // Segunda Pergunta //
quimTwo.addEventListener("click", (evt) => {
    if(evt.target === estomago) {

            quimTwo.style.display = "none"
            quimThree.style.display = "block"
    }
    
    if(evt.target === figado) {
        soma = soma + 1
        quimTwo.style.display = "none"
        quimThree.style.display = "block"
    }

    if(evt.target === pancreas) {
        quimTwo.style.display = "none"
        quimThree.style.display = "block"
    }
})


    // Terceira Pergunta //
quimThree.addEventListener("click", (evt) => {
    if(evt.target === vaporizacao) {
        quimThree.style.display = "none"
        quimFour.style.display = "block"
    }
    
    if(evt.target === fusao) {
        quimThree.style.display = "none"
        quimFour.style.display = "block"
    }
    
    if(evt.target === condesacao) {
        soma = soma + 1
        quimThree.style.display = "none"
        quimFour.style.display = "block"
    }
})



    // Quarta Pergunta //
quimFour.addEventListener("click", (evt) => {
    if(evt.target === agua) {
        soma = soma + 1
        quimFour.style.display = "none"
        quimFive.style.display = "block"
    }
    
    if(evt.target === hidro) {
        quimFour.style.display = "none"
        quimFive.style.display = "block"
    }
    
    if(evt.target === oxi) {
        quimFour.style.display = "none"
        quimFive.style.display = "block"
    }
})


    // Quinta Pergunta //
quimFive.addEventListener("click", (evt) => {
    if(evt.target === fusaoOne) {
        soma = soma + 1
        quimFive.style.display = "none"
        quimSix.style.display = "block"
    }
    
    if(evt.target === fusaoTwo) {
        quimFive.style.display = "none"
        quimSix.style.display = "block"
    }
    
    if(evt.target === fusaoThree) {
        quimFive.style.display = "none"
        quimSix.style.display = "block"
    }
})


    // Sexta Pergunta //
quimSix.addEventListener("click", (evt) => {
    if(evt.target === misturaOne) {
        quimSix.style.display = "none"
        quimSeven.style.display = "block"
    }
    
    if(evt.target === misturaTwo) {
        quimSix.style.display = "none"
        quimSeven.style.display = "block"
    }
    
    if(evt.target === misturaThree) {
        soma = soma + 1
        quimSix.style.display = "none"
        quimSeven.style.display = "block"
    }
})


    // Sétima Pergunta //
quimSeven.addEventListener("click", (evt) => {
    if(evt.target === energiaOne) {
        quimSeven.style.display = "none"
        quimEight.style.display = "block"
    }
    
    if(evt.target === energiaTwo) {
        quimSeven.style.display = "none"
        quimEight.style.display = "block"
    }
    
    if(evt.target === energiaThree) {
        soma = soma + 1
        quimSeven.style.display = "none"
        quimEight.style.display = "block"
    }
})


     // Oitava Pergunta //
quimEight.addEventListener("click", (evt) => {
    if(evt.target === formulaOne) {
        quimEight.style.display = "none"
        quimNine.style.display = "block"
    }
    
    if(evt.target === formulaTwo) {
        soma = soma + 1
        quimEight.style.display = "none"
        quimNine.style.display = "block"
    }
    
    if(evt.target === formulaThree) {
        quimEight.style.display = "none"
        quimNine.style.display = "block"
    }
})


    // Nona Pergunta //
quimNine.addEventListener("click", (evt) => {
    if(evt.target === fotoOne) {
        quimNine.style.display = "none"
        quimTen.style.display = "block"
    }
    
    if(evt.target === fotoTwo) {
        quimNine.style.display = "none"
        quimTen.style.display = "block"
    }
    
    if(evt.target === fotoThree) {
        soma = soma + 1
        quimNine.style.display = "none"
        quimTen.style.display = "block"
    }
})


     // Décima Pergunta //
quimTen.addEventListener("click", (evt) => {
    if(evt.target === dinoOne) {
        soma = soma + 1
        quimTen.style.display = "none"
        tudo.appendChild(acertando)
        acertando.style.display = "block"
        
        
    }
    
    if(evt.target === dinoTwo) {
        quimTen.style.display = "none"
        tudo.appendChild(acertando)
        acertando.style.display = "block"
        
        
    }
    
    if(evt.target === dinoThree) {
        quimTen.style.display = "none"
        tudo.appendChild(acertando)
        acertando.style.display = "block"
        
        
    }
})


// Função Pular no momento desativada // 
next.addEventListener("click" , (evt) => { 
     window.location.href = "disciplinas.html" 
  }) 
  
  
  // Função Resetar Jogo //
   limpar.addEventListener("click", () => {
     window.location.href = "quim.html"
  }) 
  
  // Função pular no momento desativada //
 /* verificar.addEventListener("click", (evt) => {
    alert(`Você acertou ${soma}`)
  }) */

  // Função de calcular pontuação //
  acertando.addEventListener("click", () => {
      acertando.style.display = "none"
      popUp.style.display = "block"
      tudo.appendChild(popUp)
      popUp.textContent = `Você acertou ${soma}`
})


  // Fisica // 

  