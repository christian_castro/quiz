let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let atuOne = document.getElementById("atuOne")
let maraOne = document.getElementById("maraOne")
let maraTwo = document.getElementById("maraTwo")
let maraThree = document.getElementById("maraThree")


// Segunda Pergunta //
let atuTwo = document.getElementById("atuTwo")
let censuraOne = document.getElementById("censuraOne")
let censuraTwo = document.getElementById("censuraTwo")
let censuraThree = document.getElementById("censuraThree")

// Terceiro Pergunta //
let atuThree = document.getElementById("atuThree")
let pixOne = document.getElementById("pixOne")
let pixTwo = document.getElementById("pixTwo")
let pixThree = document.getElementById("pixThree")

// Quarta Pergunta //
let atuFour = document.getElementById("atuFour")
let parisOne = document.getElementById("parisOne")
let parisTwo = document.getElementById("parisTwo")
let parisThree = document.getElementById("parisThree")

// Quinta Pergunta //
let atuFive = document.getElementById("atuFive")
let covidOne = document.getElementById("covidOne")
let covidTwo = document.getElementById("covidTwo")
let covidThree = document.getElementById("covidThree")

// Sexta Pergunta //
let atuSix = document.getElementById("atuSix")
let auxOne = document.getElementById("auxOne")
let auxTwo = document.getElementById("auxTwo")
let auxThree = document.getElementById("auxThree")

// Sétima Pergunta //
let atuSeven = document.getElementById("atuSeven")
let pantaOne = document.getElementById("pantaOne")
let pantaTwo = document.getElementById("pantaTwo")
let pantaThree = document.getElementById("pantaThree")

// Oitava Pergunta //
let atuEight = document.getElementById("atuEight")
let golpeOne = document.getElementById("golpeOne")
let golpeTwo = document.getElementById("golpeTwo")
let golpeThree = document.getElementById("golpeThree")

// Nona  Pergunta //
let atuNine = document.getElementById("atuNine")
let jobOne = document.getElementById("jobOne")
let jobTwo = document.getElementById("jobTwo")
let jobThree = document.getElementById("jobThree")

// Décima  Pergunta //
let atuTen = document.getElementById("atuTen")
let marsOne = document.getElementById("marsOne")
let marsTwo = document.getElementById("marsTwo")
let marsThree = document.getElementById("marsThree")

// -------------------------------------------------- //

// Cronometro // 
  let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    atuOne.style.display = "none"
    atuTwo.style.display = "none"
    atuThree.style.display = "none"
    atuFour.style.display = "none"
    atuFive.style.display = "none"
    atuSix.style.display = "none"
    atuSeven.style.display = "none"
    atuEight.style.display = "none"
    atuNine.style.display = "none"
    atuTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Tempo acabou! Você acertou ${soma}`
    
} 
} 
    // Primeira Pergunta //
    atuOne.addEventListener("click", (evt) => {
        if(evt.target === maraOne) {
            atuOne.style.display = "none"
            atuTwo.style.display = "block"
           
        } 

        if(evt.target === maraTwo) {
            soma = soma + 1
            atuOne.style.display = "none"
            atuTwo.style.display = "block"
        }
        
        if(evt.target === maraThree) {
            atuOne.style.display = "none"
            atuTwo.style.display = "block"  
        }
    })

    // Segunda Pergunta //
    atuTwo.addEventListener("click", (evt) => {
        if(evt.target === censuraOne) {
            soma = soma + 1
            atuTwo.style.display = "none"
            atuThree.style.display = "block"  
        } 

        if(evt.target === censuraTwo) {
            atuTwo.style.display = "none"
            atuThree.style.display = "block"  
        }
        
        if(evt.target === censuraThree) {
            atuTwo.style.display = "none"
            atuThree.style.display = "block"  
            
        }
    })

    // Terceira Pergunta //
    atuThree.addEventListener("click", (evt) => {
        if(evt.target === pixOne) {
            atuThree.style.display = "none"
            atuFour.style.display = "block"
        } 
        
        if(evt.target === pixTwo) {
            soma = soma + 1
            atuThree.style.display = "none"
            atuFour.style.display = "block"
        }
        
        if(evt.target === pixThree) {
            atuThree.style.display = "none"
            atuFour.style.display = "block"
            
        }
    })   


    // Quarta Pergunta //
    atuFour.addEventListener("click", (evt) => {
        if(evt.target === parisOne) {
            atuFour.style.display = "none"
            atuFive.style.display = "block"
        } 
        
        if(evt.target === parisTwo) {
            atuFour.style.display = "none"
            atuFive.style.display = "block"
        }
        
        if(evt.target === parisThree) {
            soma = soma + 1
            atuFour.style.display = "none"
            atuFive.style.display = "block"
            
        }
    }) 


    // Quinta Pergunta //
    atuFive.addEventListener("click", (evt) => {
        if(evt.target === covidOne) {
            atuFive.style.display = "none"
            atuSix.style.display = "block"
        } 
        
        if(evt.target === covidTwo) {
            atuFive.style.display = "none"
            atuSix.style.display = "block"
        }
        
        if(evt.target === covidThree) {
            soma = soma + 1
            atuFive.style.display = "none"
            atuSix.style.display = "block"
            
        }
    }) 


    // Sexta Pergunta //
    atuSix.addEventListener("click", (evt) => {
        if(evt.target === auxOne) {
            soma = soma + 1
            atuSix.style.display = "none"
            atuSeven.style.display = "block"
        } 
        
        if(evt.target === auxTwo) {
            atuSix.style.display = "none"
            atuSeven.style.display = "block"
        }
        
        if(evt.target === auxThree) {
            atuSix.style.display = "none"
            atuSeven.style.display = "block"
            
        }
    }) 


     // Sétima Pergunta //
     atuSeven.addEventListener("click", (evt) => {
        if(evt.target === pantaOne) {
            
            atuSeven.style.display = "none"
            atuEight.style.display = "block"
        } 
        
        if(evt.target === pantaTwo) {
            soma = soma + 1
            atuSeven.style.display = "none"
            atuEight.style.display = "block"
        }
        
        if(evt.target === pantaThree) {
            atuSeven.style.display = "none"
            atuEight.style.display = "block"
            
        }
    }) 

    // Oitava Pergunta //
    atuEight.addEventListener("click", (evt) => {
        if(evt.target === golpeOne) {
            atuEight.style.display = "none"
            atuNine.style.display = "block"
        } 
        
        if(evt.target === golpeTwo) {
            atuEight.style.display = "none"
            atuNine.style.display = "block"
        }
        
        if(evt.target === golpeThree) {
            soma = soma + 1
            atuEight.style.display = "none"
            atuNine.style.display = "block"         
        }
    }) 


    // Nona Pergunta //
    atuNine.addEventListener("click", (evt) => {
        if(evt.target === jobOne) {
            soma = soma + 1
            atuNine.style.display = "none"
            atuTen.style.display = "block"
        } 
        
        if(evt.target === jobTwo) {
            atuNine.style.display = "none"
            atuTen.style.display = "block"
        }
        
        if(evt.target === jobThree) {
            atuNine.style.display = "none"
            atuTen.style.display = "block"
            
        }
    }) 

     // Décima Pergunta //
     atuTen.addEventListener("click", (evt) => {
        if(evt.target === marsOne) {
            atuTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        } 
        
        if(evt.target === marsTwo) {
            soma = soma + 1
            atuTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
        
        if(evt.target === marsThree) {
            atuTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
    }) 


    let popUp = document.getElementById("popUp");
    let abrir = document.getElementById("abrir")
    let fechar = document.getElementById("fechar")
    
     // Função pular no momento desativada //
    /* fechar.addEventListener("click", () => {
        popUp.style.display = "none"
    }) */
    
    
    // Função Voltar na página inicial // 
    next.addEventListener("click" , () => { 
        window.location.href = "disciplinas.html" 
    }) 
    
    
    // Função Resetar Jogo //
    limpar.addEventListener("click", () => {
        window.location.href = "atualidades.html" 
    }) 
    
    // Função de pular no momento desativada  //
  /*  verificar.addEventListener("click", (evt) => {
        alert(`Você acertou ${soma}`)
    })  */
    
    // Função de calcular pontuação //
    acertando.addEventListener("click", () => {
        acertando.style.display = "none"
        popUp.style.display = "block"
        tudo.appendChild(popUp)
        popUp.textContent = `Você acertou ${soma}`
    })