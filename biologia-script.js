let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let bioOne = document.getElementById("bioOne")
let calcioOne = document.getElementById("calcioOne")
let calcioTwo = document.getElementById("calcioTwo")
let calcioThree = document.getElementById("calcioThree")


// Segunda Pergunta //
let bioTwo = document.getElementById("bioTwo")
let rnaOne = document.getElementById("rnaOne")
let rnaTwo = document.getElementById("rnaTwo")
let rnaThree = document.getElementById("rnaThree")

// Terceiro Pergunta //
let bioThree = document.getElementById("bioThree")
let cromoOne = document.getElementById("cromoOne")
let cromoTwo = document.getElementById("cromoTwo")
let cromoThree = document.getElementById("cromoThree")

// Quarta Pergunta //
let bioFour = document.getElementById("bioFour")
let arvoreOne = document.getElementById("arvoreOne")
let arvoreTwo = document.getElementById("arvoreTwo")
let arvoreThree = document.getElementById("arvoreThree")

// Quinta Pergunta //
let bioFive = document.getElementById("bioFive")
let relexOne = document.getElementById("relexOne")
let relexTwo = document.getElementById("relexTwo")
let relexThree = document.getElementById("relexThree")

// Sexta Pergunta //
let bioSix = document.getElementById("bioSix")
let orgaoOne = document.getElementById("orgaoOne")
let orgaoTwo = document.getElementById("orgaoTwo")
let orgaoThree = document.getElementById("orgaoThree")

// Sétima Pergunta //
let bioSeven = document.getElementById("bioSeven")
let fossilOne = document.getElementById("fossilOne")
let fossilTwo = document.getElementById("fossilTwo")
let fossilThree = document.getElementById("fossilThree")

// Oitava Pergunta //
let bioEight = document.getElementById("bioEight")
let coreOne = document.getElementById("coreOne")
let coreTwo = document.getElementById("coreTwo")
let coreThree = document.getElementById("coreThree")

// Nona  Pergunta //
let bioNine = document.getElementById("bioNine")
let musculoOne = document.getElementById("musculoOne")
let musculoTwo = document.getElementById("musculoTwo")
let musculoThree = document.getElementById("musculoThree")

// Décima  Pergunta //
let bioTen = document.getElementById("bioTen")
let bactOne = document.getElementById("bactOne")
let bactTwo = document.getElementById("bactTwo")
let bactThree = document.getElementById("bactThree")

// -------------------------------------------------- //

// Cronometro // 
  let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    bioOne.style.display = "none"
    bioTwo.style.display = "none"
    bioThree.style.display = "none"
    bioFour.style.display = "none"
    bioFive.style.display = "none"
    bioSix.style.display = "none"
    bioSeven.style.display = "none"
    bioEight.style.display = "none"
    bioNine.style.display = "none"
    bioTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`

}
} 

    // Primeira Pergunta //
    bioOne.addEventListener("click", (evt) => {
        if(evt.target === calcioOne) {
            bioOne.style.display = "none"
            bioTwo.style.display = "block"
           
        } 

        if(evt.target === calcioTwo) {
            bioOne.style.display = "none"
            bioTwo.style.display = "block"
        }
        
        if(evt.target === calcioThree) {
            soma = soma + 1
            bioOne.style.display = "none"
            bioTwo.style.display = "block"  
        }
    })

    // Segunda Pergunta //
    bioTwo.addEventListener("click", (evt) => {
        if(evt.target === rnaOne) {
            bioTwo.style.display = "none"
            bioThree.style.display = "block"  
        } 

        if(evt.target === rnaTwo) {
            soma = soma + 1
            bioTwo.style.display = "none"
            bioThree.style.display = "block"  
        }
        
        if(evt.target === rnaThree) {
            bioTwo.style.display = "none"
            bioThree.style.display = "block"  
            
        }
    })

    // Terceira Pergunta //
    bioThree.addEventListener("click", (evt) => {
        if(evt.target === cromoOne) {
            soma = soma + 1
            bioThree.style.display = "none"
            bioFour.style.display = "block"
        } 
        
        if(evt.target === cromoTwo) {
            bioThree.style.display = "none"
            bioFour.style.display = "block"
        }
        
        if(evt.target === cromoThree) {
            bioThree.style.display = "none"
            bioFour.style.display = "block"
            
        }
    })   


    // Quarta Pergunta //
    bioFour.addEventListener("click", (evt) => {
        if(evt.target === arvoreOne) {
            soma = soma + 1
            bioFour.style.display = "none"
            bioFive.style.display = "block"
        } 
        
        if(evt.target === arvoreTwo) {
            bioFour.style.display = "none"
            bioFive.style.display = "block"
        }
        
        if(evt.target === arvoreThree) {
            bioFour.style.display = "none"
            bioFive.style.display = "block"
            
        }
    }) 


    // Quinta Pergunta //
    bioFive.addEventListener("click", (evt) => {
        if(evt.target === relexOne) {
            bioFive.style.display = "none"
            bioSix.style.display = "block"
        } 
        
        if(evt.target === relexTwo) {
            soma = soma + 1
            bioFive.style.display = "none"
            bioSix.style.display = "block"
        }
        
        if(evt.target === relexThree) {
            bioFive.style.display = "none"
            bioSix.style.display = "block"
            
        }
    }) 


    // Sexta Pergunta //
    bioSix.addEventListener("click", (evt) => {
        if(evt.target === orgaoOne) {
            bioSix.style.display = "none"
            bioSeven.style.display = "block"
        } 
        
        if(evt.target === orgaoTwo) {
            bioSix.style.display = "none"
            bioSeven.style.display = "block"
        }
        
        if(evt.target === orgaoThree) {
            soma = soma + 1
            bioSix.style.display = "none"
            bioSeven.style.display = "block"
            
        }
    }) 


     // Sétima Pergunta //
     bioSeven.addEventListener("click", (evt) => {
        if(evt.target === fossilOne) {
            
            bioSeven.style.display = "none"
            bioEight.style.display = "block"
        } 
        
        if(evt.target === fossilTwo) {
            soma = soma + 1
            bioSeven.style.display = "none"
            bioEight.style.display = "block"
        }
        
        if(evt.target === fossilThree) {
            bioSeven.style.display = "none"
            bioEight.style.display = "block"
            
        }
    }) 

    // Oitava Pergunta //
    bioEight.addEventListener("click", (evt) => {
        if(evt.target === coreOne) {
            bioEight.style.display = "none"
            bioNine.style.display = "block"
        } 
        
        if(evt.target === coreTwo) {
            bioEight.style.display = "none"
            bioNine.style.display = "block"
        }
        
        if(evt.target === coreThree) {
            soma = soma + 1
            bioEight.style.display = "none"
            bioNine.style.display = "block"         
        }
    }) 


    // Nona Pergunta //
    bioNine.addEventListener("click", (evt) => {
        if(evt.target === musculoOne) {
            bioNine.style.display = "none"
            bioTen.style.display = "block"
        } 
        
        if(evt.target === musculoTwo) {
            soma = soma + 1
            bioNine.style.display = "none"
            bioTen.style.display = "block"
        }
        
        if(evt.target === musculoThree) {
            bioNine.style.display = "none"
            bioTen.style.display = "block"
            
        }
    }) 

     // Décima Pergunta //
     bioTen.addEventListener("click", (evt) => {
        if(evt.target === bactOne) {
            bioTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        } 
        
        if(evt.target === bactTwo) {
            soma = soma + 1
            bioTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
        
        if(evt.target === bactThree) {
            bioTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
        }
    }) 


    let popUp = document.getElementById("popUp");
    let abrir = document.getElementById("abrir")
    let fechar = document.getElementById("fechar")
    
     // Função pular no momento desativada //
    /* fechar.addEventListener("click", () => {
        popUp.style.display = "none"
    }) */
    
    
    // Função Voltar na página inicial // 
    next.addEventListener("click" , () => { 
        window.location.href = "disciplinas.html" 
    }) 
    
    
    // Função Resetar Jogo //
    limpar.addEventListener("click", () => {
        window.location.href = "biologia.html" 
    }) 
    
    // Função de pular no momento desativada  //
  /*  verificar.addEventListener("click", (evt) => {
        alert(`Você acertou ${soma}`)
    })  */
    
    // Função de calcular pontuação //
    acertando.addEventListener("click", () => {
        acertando.style.display = "none"
        popUp.style.display = "block"
        tudo.appendChild(popUp)
        popUp.textContent = `Você acertou ${soma}`
    })
    