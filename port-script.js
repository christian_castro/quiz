let next = document.getElementById("next")
let voltar = document.getElementById("voltar")
let buttons = document.getElementById("buttons")

let acertando = document.getElementById("acertando")
let tudo = document.getElementById("tudo")
let resposta = document.getElementById("resposta")
let span = document.getElementById("span")


let res = document.getElementById("res")
let restworld = document.getElementById("restwo")
let limpar = document.getElementById("limpar")
let soma = 0

let body = document.querySelector("body")


// Primeira Pergunta //
let portOne = document.getElementById("portOne")
let singularOne = document.getElementById("singularOne")
let singularTwo = document.getElementById("singularTwo")
let singularThree = document.getElementById("singularThree")


// Segunda Pergunta //
let portTwo = document.getElementById("portTwo")
let classeOne = document.getElementById("classeOne")
let classeTwo = document.getElementById("classeTwo")
let classeThree = document.getElementById("classeThree")

// Terceiro Pergunta //
let portThree = document.getElementById("portThree")
let escritaOne = document.getElementById("escritaOne")
let escritaTwo = document.getElementById("escritaTwo")
let escritaThree = document.getElementById("escritaThree")

// Quarta Pergunta //
let portFour = document.getElementById("portFour")
let oracaoOne = document.getElementById("oracaoOne")
let oracaoTwo = document.getElementById("oracaoTwo")
let oracaoThree = document.getElementById("oracaoThree")

// Quinta Pergunta //
let portFive = document.getElementById("portFive")
let ironiaOne = document.getElementById("ironiaOne")
let ironiaTwo = document.getElementById("ironiaTwo")
let ironiaThree = document.getElementById("ironiaThree")

// Sexta Pergunta //
let portSix = document.getElementById("portSix")
let comparacaoOne = document.getElementById("comparacaoOne")
let comparacaoTwo = document.getElementById("comparacaoTwo")
let comparacaoThree = document.getElementById("comparacaoThree")

// Sétima Pergunta //
let portSeven = document.getElementById("portSeven")
let linguagemOne = document.getElementById("linguagemOne")
let linguagemTwo = document.getElementById("linguagemTwo")
let linguagemThree = document.getElementById("linguagemThree")

// Oitava Pergunta //
let portEight = document.getElementById("portEight")
let formaOne = document.getElementById("formaOne")
let formaTwo = document.getElementById("formaTwo")
let formaThree = document.getElementById("formaThree")

// Nona  Pergunta //
let portNine = document.getElementById("portNine")
let formaFour = document.getElementById("formaFour")
let formaFive = document.getElementById("formaFive")
let formaSix = document.getElementById("formaSix")

// Décima  Pergunta //
let portTen = document.getElementById("portTen")
let formaSeven = document.getElementById("formaSeven")
let formaEight = document.getElementById("formaEight")
let formaNine = document.getElementById("formaNine")

// -------------------------------------------------- //

// Cronometro // 
let cronometroJs = document.getElementById("cronometroJs")

let segundos = 60;

let cronometro;

cronometroJs.addEventListener("click", () => {
   cronometro = setInterval(() => { 
       timer()
    },1000)
})

function timer() {

    segundos = segundos - 1;
    
    counter.style.display = "block";
    counter.innerText = segundos;  
    if(segundos <= 0) {
    counter.style.display = "none";
    portOne.style.display = "none"
    portTwo.style.display = "none"
    portThree.style.display = "none"
    portFour.style.display = "none"
    portFive.style.display = "none"
    portSix.style.display = "none"
    portSeven.style.display = "none"
    portEight.style.display = "none"
    portNine.style.display = "none"
    portTen.style.display = "none"
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`
    
    
    // Zerar Cronometro // 
} 
} 
    // Primeira Pergunta //
    portOne.addEventListener("click", (evt) => {
        if(evt.target === singularOne) {
            portOne.style.display = "none"
            portTwo.style.display = "block"
        } 

        if(evt.target === singularTwo) {
            soma = soma + 1
            portOne.style.display = "none"
            portTwo.style.display = "block"    
        }

        if(evt.target === singularThree) {
            portOne.style.display = "none"
            portTwo.style.display = "block"
            
        }
    })

         // Segunda Pergunta //
    portTwo.addEventListener("click", (evt) => {
        if(evt.target === classeOne) {   
            portTwo.style.display = "none"
            portThree.style.display = "block"
        } 

        if(evt.target === classeTwo) {   
            portTwo.style.display = "none"
            portThree.style.display = "block"
        }
        
        if(evt.target === classeThree) {
            soma = soma + 1
            portTwo.style.display = "none"
            portThree.style.display = "block"
        }
    })

      // Terceiro Pergunta //
      portThree.addEventListener("click", (evt) => {
        if(evt.target === escritaOne) {   
            portThree.style.display = "none"
            portFour.style.display = "block"
        } 

        if(evt.target === escritaTwo) {
            soma = soma + 1
            portThree.style.display = "none"
            portFour.style.display = "block"
        }
        
        if(evt.target === escritaThree) {    
            portThree.style.display = "none"
            portFour.style.display = "block"
        }
    })


    // Quarta Pergunta //
    portFour.addEventListener("click", (evt) => {
        if(evt.target === oracaoOne) {        
            portFour.style.display = "none"
            portFive.style.display = "block"
        } 

        if(evt.target === oracaoTwo) {      
            portFour.style.display = "none"
            portFive.style.display = "block"
        }
        
        if(evt.target === oracaoThree) {
            soma = soma + 1
            portFour.style.display = "none"
            portFive.style.display = "block"
        }
    })


    // Quinta Pergunta //
    portFive.addEventListener("click", (evt) => {
        if(evt.target === ironiaOne) {       
            portFive.style.display = "none"
            portSix.style.display = "block"
        } 

        if(evt.target === ironiaTwo) {
            soma = soma + 1
            portFive.style.display = "none"
            portSix.style.display = "block"
        }
        
        if(evt.target === ironiaThree) {     
            portFive.style.display = "none"
            portSix.style.display = "block"
        }
    })


    // Sexta Pergunta //
    portSix.addEventListener("click", (evt) => {
        if(evt.target === comparacaoOne) {
            soma = soma + 1
            portSix.style.display = "none"
            portSeven.style.display = "block"
        } 
        
        if(evt.target === comparacaoTwo) {      
            portSix.style.display = "none"
            portSeven.style.display = "block"
        }
        
        if(evt.target === comparacaoThree) {  
            portSix.style.display = "none"
            portSeven.style.display = "block"
        }
    })

     // Sétima Pergunta //
     portSeven.addEventListener("click", (evt) => {
        if(evt.target === linguagemOne) {   
            portSeven.style.display = "none"
            portEight.style.display = "block"
        } 
        
        if(evt.target === linguagemTwo) {       
            portSeven.style.display = "none"
            portEight.style.display = "block"
        }
        
        if(evt.target === linguagemThree) {
            soma = soma + 1
            portSeven.style.display = "none"
            portEight.style.display = "block"
        }
    })

    // Oitava Pergunta //
    portEight.addEventListener("click", (evt) => {
        if(evt.target === formaOne) {          
            portEight.style.display = "none"
            portNine.style.display = "block"
        } 
        
        if(evt.target === formaTwo) {           
            portEight.style.display = "none"
            portNine.style.display = "block"
        }
        
        if(evt.target === formaThree) {
            soma = soma + 1
            portEight.style.display = "none"
            portNine.style.display = "block"
        }
    })

        // Nona Pergunta //
       portNine.addEventListener("click", (evt) => {
        if(evt.target === formaFour) {
            soma = soma + 1
            portNine.style.display = "none"
            portTen.style.display = "block"
        } 
        
        if(evt.target === formaFive) {           
            portNine.style.display = "none"
            portTen.style.display = "block"
        }
        
        if(evt.target === formaSix) {
            
            portNine.style.display = "none"
            portTen.style.display = "block"
        }
    })

    // Décima Pergunta //
    portTen.addEventListener("click", (evt) => {
        if(evt.target === formaSeven) {
            portTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        } 
        
        if(evt.target === formaEight) {
            portTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
        
        if(evt.target === formaNine) {
            soma = soma + 1
            portTen.style.display = "none"
            tudo.appendChild(acertando)
            acertando.style.display = "block"
            
        }
    })

// Função Voltar na página inicial // 
next.addEventListener("click" , (evt) => { 
    window.location.href = "disciplinas.html" 
  }) 
  
  
  // Função Resetar Jogo //
   limpar.addEventListener("click", () => {
     window.location.href = "port.html"
  }) 
  
  // Função de pular no momento desativada  //
 /* verificar.addEventListener("click", (evt) => {
    alert(`Você acertou ${soma}`)
  }) */


// Função de calcular pontuação //
acertando.addEventListener("click", () => {
    acertando.style.display = "none"
    popUp.style.display = "block"
    tudo.appendChild(popUp)
    popUp.textContent = `Você acertou ${soma}`
})
  // Fisica // 

  